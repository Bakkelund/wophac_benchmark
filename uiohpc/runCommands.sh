#!/bin/sh
#!/bin/sh
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

DONEFILE=./uiohpc/log/done.txt
DOINGFILE=./uiohpc/log/doing.txt
LOGFILE=./uiohpc/log/shell.log

INFILE=$1

echo '##'                      >> $LOGFILE
echo '##' $(date)              >> $LOGFILE
echo '##' Reading from $INFILE >> $LOGFILE
echo '##'                      >> $LOGFILE

get_len() {
    echo `wc -l $INFILE | sed 's/ *\([^ ]*\).*/\1/'`
}

while [ "$(get_len)" -ge "1" ]; do
    # Read the next line
    line=`head -n 1 $INFILE`

    # Remove the line from the file
    tail -n +2 "$INFILE" > "$INFILE.tmp" && mv "$INFILE.tmp" "$INFILE"

    echo $line > $DOINGFILE

    echo '##'                >> $LOGFILE
    echo '##' $(date)        >> $LOGFILE
    echo '##' Runnung $line  >> $LOGFILE
    echo '##'                >> $LOGFILE

    echo Running $line
    python3 -m comparative_analysis $line 2>&1 | tee -a $LOGFILE

    echo $line >> $DONEFILE
done
