# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

import numpy as np
import numpy.random as rnd
import time

rnd.seed(int(time.time()))

CPP_MAX_UINT = 4294967294

ccs      = [6,7]
a0       = 0.0
a1       = 1.0
nalphas  = 50
minDegs  = [1.00]
locs     = [0.075]
scales   = [0.15]
L        = 'complete'
iters    = 50
NM       = [(5,4)]
ncpu     = 30
ncpp     = 30
ncppcpu  = 1

def lnk(L):
    return {'single':'SL', 'average':'AL', 'complete':'CL'}[L]

def fltstr(s):
    return ('%0.3f' % s).replace('.','')

for cc in ccs:
    for n,m in NM:
        for loc in locs:
            for var in scales:
                for minDeg in minDegs:
                    zeed  = rnd.randint(CPP_MAX_UINT)
                    fbase = 'cc%d_n%d_m%d_minDeg%s_mu%s_var%s' % (cc, n, m, fltstr(minDeg), fltstr(loc),fltstr(var))
                    outfn = 'uiohpc/data/bioint01_' + fbase + '.json'

                    params = '--ccs:%d --n0:%d --m:%d --minDeg:%1.2f --mu:%1.3f --var:%1.3f --L:%s --a0:%1.3f --a1:%1.3f --nalphas:%d --iters:%d --fname:%s --ncpu:%d --ncpp:%d --ncppcpu:%d --seed:%d' % \
                        (cc, n, m, minDeg, loc, var, L, a0, a1, nalphas, iters, outfn, ncpu, ncpp, ncppcpu, zeed)
        

                    print(params)
            
            
