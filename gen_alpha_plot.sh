#!/bin/sh
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

TODIR=pgfcsvs
DATADIR=down_data/bioint01/selected/data/

if test ! -d ${TODIR}; then
    echo ${TODIR} is not a directory.
    exit 42
fi

source ./setEnv.sh

alphaFile=bioint01_cc6_n5_m4_minDeg1000_mu0075_var0150.json
outBase=alphas

for qual in ari oari loops; do
    echo $qual
    python3 -m alpha_to_csv qual=$qual ofname="${TODIR}/${outBase}_${qual}.csv" $DATADIR/$alphaFile
done
