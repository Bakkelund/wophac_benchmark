#!/bin/sh
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

TODIR=pgfcsvs
DATADIR=down_data/bioint01/selected/data/

if test ! -d ${TODIR}; then
    echo ${TODIR} is not a directory.
    exit 42
fi

source ./setEnv.sh

do_dump() {
    for qual in ari oari loops; do
	for m in ophac wophac dgM scipy scipyM; do
	    python3 -m dim_to_csv qual=$qual dim=$BASE ofname="${TODIR}/${BASE}_${m}_${qual}.csv" method=$m $FNAMES
	done
    done
}


echo Variance
FNAMES=`ls ${DATADIR}/bioint01_cc6_n5_m4_minDeg1000_mu0075_var*0.json | xargs`
BASE=var
do_dump

echo Location
FNAMES=`ls ${DATADIR}/bioint01_cc6_n5_m4_minDeg1000_mu*_var0150.json | xargs`
BASE=mu
do_dump

echo Minimum degree
FNAMES=`ls ${DATADIR}/bioint01_cc6_n5_m4_minDeg*_mu0075_var0150.json | xargs`
BASE=minDeg
do_dump

exit 0

for qual in ari oari loops; do
    echo $qual
    python3 -m alpha_to_csv qual=$qual ofname="${TODIR}/${outBase}_${qual}.csv" $DATADIR/$alphaFile
done
