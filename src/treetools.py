# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def _getLogger(x):
    import logging
    return logging.getLogger(__name__ + '.' + x.__name__)

def tuplify(T):
    '''
    Takes a numpy array split tree and makes it tuplic.
    '''
    T2 = {}
    for P,(A,B) in T.items():
        T2[P] = ((tuple(A),tuple(B)))

    return T2

def _split_value(A,B,F):
    from itertools import product
    val = 0
    for a,b in product(A,B):
        val += F[a,b]

    return (len(A)+len(B))*val

def tree_value(T,F):
    S = splits(T)
    v = 0
    for A,B in S:
        sv = max(_split_value(A,B,F), _split_value(B,A,F))
        v += sv

    return v

def equal(T1,T2):
    S1 = splits(T1)
    S2 = splits(T2)
    return S1 == S2

def splits(T):
    zplits = set([s for s in T.values()])
    return sorted(map(tuple,map(sorted,zplits)))

def isAcyclic(T,W):
    from itertools import product
    import numpy as np
    
    zplits = splits(T)
    for A,B in zplits:
        sgns = np.array([W[a,b] for a,b in product(A,B)])
        if not (np.all(sgns >= 0) or np.all(sgns <= 0)):
            return False

    return True

def treeToPartitionSequence(T,S,W):
    import numpy        as np
    log = _getLogger(treeToPartitionSequence)
    
    # Maximum index == number of elts
    N = np.max(np.concatenate([np.concatenate((a,b)) for (a,b) in T.values()]))

    # Singleton partition
    Q      = [(x,) for x in range(N+1)]

    # Pairs sorted according to split value, the smallest at the back
    F      = (1-S) + (W-W.T)
    calc   = lambda a,b : np.max([_split_value(a,b,F),_split_value(b,a,F)])

    partitions = [list(Q)]
    merges     = []
    values     = []
    zplits     = splits(T)
    zplits     = [(s,calc(*s)) for s in zplits]
    zplits     = sorted(zplits, key=lambda x : x[-1], reverse=True)
    
    while len(zplits) > 0:
        (a,b),v = zplits.pop()
        log.debug('merging %s + %s at value %1.3f', a, b, v)

        try:
            ia  = Q.index(a)
            ib  = Q.index(b)
        except ValueError as e:
            print(e)
            print('value', v)
            print('Q',Q)
            raise e
        
        merges.append((ia,ib))
        values.append(v)
        Q.remove(a)
        Q.remove(b)
        Q.append(tuple(sorted(np.concatenate([a,b]))))
        partitions.append(sorted(Q))
        log.debug('partition %s at value %1.3f', partitions[-1], v)

    assert len(Q) == 1

    return partitions,merges,values
        
def treeToHC(T):
    import numpy        as np
    from itertools      import product
    
    splits = set()
    for a,b in T.values():
        splits.add((tuple(a),tuple(b)))
        
    splits = sorted(splits, key=lambda x : len(x[0]) + len(x[1]))
    n      = np.max(np.concatenate(splits[-1])) + 1
    U      = np.full((n,n), np.infty, dtype=float)
    np.fill_diagonal(U, 0.0)
    for a,b in splits:
        u = len(a) + len(b) - 1
        for i,j in product(a,b):
            U[i,j] = np.min([U[i,j], u])
            U[j,i] = U[i,j]
            
    assert np.all(U < np.infty)
    
    return splits,U

def treeToOphacAC(T):
    import ophac.dtypes as dt
    splits,U = treeToHC(T)
    splits   = [[list(a),list(b)] for a,b in splits]
    N        = U.shape[0]
    joins    = []
    dists    = []
    idmap    = [[i] for i in range(N)]
    for a,b in splits:
        ia = idmap.index(a)
        ib = idmap.index(b)
        d  = U[a[0],b[0]]
        joins.append([ia,ib])
        dists.append(d)

        idmap[ia] = sorted(a + b)
        del idmap[ib]

    return dt.AC(joins,dists)
