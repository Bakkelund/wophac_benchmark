# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2022 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

import numpy as np
from itertools import product
import timer

def _getLogger(x):
    import logging
    return logging.getLogger(__name__ + '.' + x.__name__)

def cut_value(A,B,w):
    return np.sum([w[a,b] for a,b in product(A,B)])

def asym_cut_value(A,B,w):
    return np.sum([w[a,b] - w[b,a] for a,b in product(A,B)])

def density_cut_value(A,B,w):
    return cut_value(A,B,w)/(len(A)*len(B))

def density_asym_cut_value(A,B,w):
    return asym_cut_value(A,B,w)/(len(A)*len(B))

def swap_cut_value(A,B,a,b,w,cutter):
    A1 = (A - {a}) | {b}
    B1 = (B - {b}) | {a}
    return cutter(A1,B1,w)

def shuffle_max_cut(A,B,w,cutter):
    A0      = set(A)
    B0      = set(B)
    v0      = cutter(A,B,w)
    changed = True
    while changed:
        prd  = product(A,B)
        a,b  = next(prd)
        v1   = swap_cut_value(A0,B0,a,b,w,cutter)
        best = [(a,b)]
        for a,b in prd:
            v = swap_cut_value(A0,B0,a,b,w,cutter)
            if v > v1:
                best = [(a,b)]
                v1   = v
            elif v == v1:
                best.append((a,b))

        if v1 > v0:
            a,b = best[np.random.randint(len(best))]
            A0  = (A0 - {a}) | {b}
            B0  = (B0 - {b}) | {a}
            v0  = v1
        else:
            changed = False

    return A0,B0,v0

def cut(w,X=None,cutter=density_cut_value):
    tim = timer.timer()
    log = _getLogger(cut)
    
    if not X:
        X = set(np.arange(w.shape[0]))
        
    S = np.random.permutation(list(X))
    n = len(S)//2
    A = set(S[:n])
    B = set(S[n:])
    A,B,v = shuffle_max_cut(A,B,w,cutter)
    log.debug('time(%d) =  %1.4f s.', len(X), tim)
    return A,B,v


def hierarchy(w,cutter=density_cut_value):
    tim = timer.timer()
    log = _getLogger(hierarchy)
    X   = set(range(w.shape[0]))
    T   = {}
    XX  = [X]
    while len(XX) > 0:
        S = XX.pop()
        A,B,v = cut(w=w,X=S,cutter=cutter)

        # insert into tree
        ab = tuple(sorted((tuple(sorted(A)),tuple(sorted(B)))))
        for (a,b) in product(A,B):
            (a,b) = sorted((a,b))
            T[(a,b)] = ab

        # Push new clusters back if they are not singletons
        if len(A) > 1:
            XX.append(A)
        if len(B) > 1:
            XX.append(B)

    log.debug('time(%d) = %1.3f s.', w.shape[0], tim)
            
    return T
            
