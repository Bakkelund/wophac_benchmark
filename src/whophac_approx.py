# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def dir_sparse_cut(F):
    import cut_iterator
    n  = len(F)
    X  = list(range(n))
    bc = (X,[])
    bv = 10e10
    for A,B in cut_iterator.cuts(X):
        (A,B),cv = _oriented_cut_value(F,A,B)
        if cv < bv:
            bv = cv
            bc = (A,B)
     
    return bc

_cutter = dir_sparse_cut


def _oriented_cut_value(F,A,B):
    import numpy as np
    from itertools import product
    I,J = zip(*product(A,B))
    #print(f'{np.sum(F[I,J])=} {np.sum(F[J,I])=}')
    cv1  = np.sum(F[I,J])/(len(A)*len(B))
    cv2  = np.sum(F[J,I])/(len(A)*len(B))
    assert cv1 != 0 or cv2 != 0
    if cv1 > 0 and (cv2 == 0 or cv1 < cv2):
        return (A,B),cv1
    else:
        return (B,A),cv2

def set_cutter(X):
    print('Setting cutter to ', X)
    global _cutter
    _cutter = X

def hierarchy(S, W, alpha=0.5, cutter=None):
    '''
    s - similarity measure (NxN numpy in [0,1])
    w - relaxed relation (NxN numpy in [0,1])
    alpha - weighting between w and s. If alpha=0, then s is ignored, and
            if alpha=1, then w is ignored. Must be in the range [0,1].
    cutter - The directed sparsest cut approximation. Defaults to `dir_sparse_cut`.

    The method returns a dict mapping pairs (a,b) of elements, where a<b, to
    the split (A,B) of subsets of {0,...,N-1} in which a and b were split apart.
    This data structure is generally referred to as a "tree". There are some
    utility functions for trees in the file treetools.py.
    '''
    import numpy as np
    from   itertools import product

    if not cutter:
        cutter = _cutter
    
    assert S.shape == W.shape
    
    n  = len(S)
    XX = [list(range(n))]
    T  = {}
    while len(XX) > 0:
        X     = XX.pop()
        s,inv = project(S,X)
        w,inv = project(W,X)
        g     = w - w.T
        f     = (1-s) * alpha + g * (1-alpha)
        fd    = 2-f
        a,b   = cutter(fd)
        A     = inv(a)
        B     = inv(b)

        #print(X, '-->', A, B)
        
        if len(A) > 1:
            XX.append(A)
        if len(B) > 1:
            XX.append(B)

        ab = tuple(sorted((tuple(sorted(A)),tuple(sorted(B)))))
        for (a,b) in product(A,B):
            (a,b) = sorted((a,b))
            T[(a,b)] = ab

    return T
            
def project(A,X):
    from itertools import product
    Y = list(sorted(X))
    P = product(Y,Y)
    I,J = zip(*P)
    B = A[I,J].reshape((len(X),len(X)))
    inv = lambda x : [Y[i] for i in x]
    return B,inv
    
def pareto_front(S,W,cutter,dMin=0.01):
    import treetools as tt

    T0,v0 = _pareto_sub(S,W,0.0,cutter)
    T1,v1 = _pareto_sub(S,W,1.0,cutter)

    if tt.equal(T0,T1):
        return [(0.0,T0,v0)]

    front     = {0.0:(T0,v0), 1.0:(T1,v1)}
    intervals = [(0.0,1.0)]
    while len(intervals) > 0:
        a0,a1 = intervals.pop()
        a     = (a0+a1)/2
        T,v   = _pareto_sub(S,W,a,cutter)
        T0,v0 = front[a0]
        T1,v1 = front[a1]
        s0    = len(intervals) + 1

        if not tt.equal(T,T0):
            front[a] = (T,v)
            if a - a0 > dMin:
                intervals.append([a0,a])
            print('Inserted', [a0,a])

        if not tt.equal(T,T1):
            front[a] = (T,v)
            if a1 - a > dMin:
                intervals.append([a,a1])
            print('Inserted', [a,a1])
            
        print('delta intervals:', len(intervals) - s0)

    return [(a,*front[a]) for a in sorted(front)]
    

def _pareto_sub(S,W,alpha,cutter):
    import treetools as tt
    T = hierarchy(S,W,alpha,cutter)
    F = (1-S) + (W-W.T)
    v = tt.tree_value(T,F)
    return T,v
