# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

import numpy as np

def _toClst(A):
    '''
    Converts A from a basic list-based partition to a clusim Clustering.
    '''
    from clusim.clustering import Clustering
    c = {}
    for i in range(len(A)):
        for x in A[i]:
            c[x] = [i]

    return Clustering(elm2clu_dict=c)

def randIndex(A,B):
    import clusim.sim as sim
    return sim.rand_index(_toClst(A),_toClst(B))

def adjustedRandIndexAllPartitions(A,B):
    import clusim.sim as sim
    return sim.adjrand_index(_toClst(A),_toClst(B),'all')

def oneSidedARI(G, A, mode='all'):
    '''
    G is the gold standard.
    mode - one of "all" or "k"
    '''
    import clusim.sim as sim
    if len(G) == 1 and len(A) == 1:
        assert len(G[0]) == len(A[0]) # Partitions are over different sets!
        return 1.0

    param = None
    if mode == 'all':
        param = 'all1'
    elif mode == 'k':
        param = 'num1'
    else:
        raise Exception('Unknown mode: "%s" (must be one of "all" or "k").' % mode)
        
    return sim.adjrand_index(_toClst(A),_toClst(G),param)

def mutualInformation(A,B):
    import clusim.sim as sim
    return sim.mi(_toClst(A),_toClst(B))

def _entropy(A):
    ai = np.array([len(a) for a in A])
    N  = np.sum(ai)
    return -np.sum(ai/N*np.log2(ai/N))

def oneSidedAMI(G,A):
    '''
    G is the gold standard.
    '''
    import clusim.sim as sim
    cG = _toClst(G)
    cA = _toClst(A)
    return sim.adj_mi(cA,cG,'num1','max')

def orderARI(O1,O2):
    '''
    Based on "A note on using the adjusted Rand index for link prediction in networks"
    (2015) by Hoffman, Steinley and Brusco.
    https://www-sciencedirect-com.ezproxy.uio.no/science/article/pii/S0378873315000210
    O1,O2 - order matrices.
    '''
    import numpy as np
    N   = len(O1)
    a  = np.array([np.dot(  O1[i] ,  O2[i]  ) for i in np.arange(N)])
    b  = np.array([np.dot(  O1[i] , 1-O2[i] ) for i in np.arange(N)])
    c  = np.array([np.dot( 1-O1[i],  O2[i]  ) for i in np.arange(N)])
    d  = np.array([np.dot( 1-O1[i], 1-O2[i] ) for i in np.arange(N)])

    numer = 2*(a*d-b*c)
    denom = (a+b)*(b+d)+(a+c)*(c+d)
    dz    = denom == 0.0
    assert np.all(dz <= (numer == 0.0))
    denom[dz] = 1.0
    numer[dz] = 1.0

    ARI = numer/denom
    
    return np.mean(ARI)

def loopFactor(qq):
    '''
    qq - quivers
    '''
    import ophac.dtypes as dt
    Q = dt.Quivers(qq).transitiveClosure(lenient=True).quivers

    loops = 0
    for i in range(len(Q)):
        if i in Q[i]:
            loops += 1

    return 1.0 - loops/len(qq)
        

    
if __name__ == '__main__':
    print('Running tests.')
    import unittest as ut

    class TestCaseX(ut.TestCase):

        def assertClose(self,a,b,thresh=1e-5,msg=''):
            if not np.all(np.abs(a-b) <= thresh):
                raise AssertionError('Not close: ' +
                                     ('%s differs from %s by more than %s.' %
                                      (a,b,thresh)) + msg
                )

    class TestLoopFactor(TestCaseX):

        def testSimple(self):
            qq = [[1,2],[3],[1],[2]]
            self.assertEqual(1-3/4, loopFactor(qq))

        def testHamiltonianCycle(self):
            qq = [[1],[2],[3],[4],[5],[0]]
            self.assertEqual(0, loopFactor(qq))

        def testNoCycle(self):
            qq = [[1],[2],[]]
            self.assertEqual(1, loopFactor(qq))
    
    class TestRandIndex(TestCaseX):

        def testRI(self):
            X = list(range(5))
            S = [[x] for x in X]
            A = [[0,1],[2],[3,4]]
            B = [[0,1],[2,3,4]]
            C = [[0,1,2,3],[4]]

            self.assertEqual(0, randIndex(S,[X]), 'S-[X]')
            self.assertEqual(1, randIndex(S,S), 'S-S')
            self.assertEqual(1, randIndex(A,A), 'A-A')

            self.assertEqual(8/10, randIndex(A,B), 'RI(A,B)')
            self.assertEqual(4/10, randIndex(A,C), 'RI(A,C)')

            self.assertEqual(1.0, randIndex([[1,2,3]],[[1,2,3]]), 'RI([X],[X])')

        def testARI_All(self):
            X = list(range(5))
            S = [[x] for x in X]
            A = [[0,1],[2],[3,4]]
            B = [[0,1],[2,3,4]]
            C = [[0,1,2,3],[4]]

            self.assertTrue(0 > adjustedRandIndexAllPartitions(S,[X]), 'S-[X]')
            self.assertEqual(1, adjustedRandIndexAllPartitions(S,S), 'S-S')
            self.assertEqual(1, adjustedRandIndexAllPartitions(A,A), 'A-A')

            self.assertTrue(0 < adjustedRandIndexAllPartitions(A,B), 'ARI(A,B)')
            self.assertTrue(0 > adjustedRandIndexAllPartitions(A,C), 'ARI(A,C)')

            self.assertEqual(1.0, adjustedRandIndexAllPartitions([[1,2,3]],[[1,2,3]]),
                             'ARI([X],[X])')
            
            
        def testARI(self):
            '''
            Test checks that ordering is as according to ordering in the paper
            by Gates and Ahn (2017, JMLR).
            '''
            W = [[1,2,3,4,5],[6,7,8,9,10],[11,12,13,14,15],[16,17,18,19,20]]
            X = [[1,2,3,4,5,6],[7,8,9,10],[11,12,13,14,15],[16,17,18,19,20]]
            Y = [[1,2],[3,4],[5,6],[7,8],[9,10],
                 [11,12],[13,14],[15,16],[17,18],[19,20]]
            Z = [[1],[2],[3],[4],[5],[6],[7],[8],[9,10],
                 [11,12,13,14,15,16,17,18,19,20]]

            riWX = randIndex(W,X)
            riWY = randIndex(W,Y)
            riWZ = randIndex(W,Z)
            riXY = randIndex(X,Y)
            riXZ = randIndex(X,Z)
            riYZ = randIndex(Y,Z)

            self.assertTrue(riXZ < riWZ, 'riXZ < riWZ')
            self.assertTrue(riWZ <= riYZ, 'riWZ <= riYZ')
            self.assertTrue(riYZ < riWY, 'riYZ < riWY')
            self.assertTrue(riWY < riXY, 'riWY < riXY')
            self.assertTrue(riXY < riWX, 'riXY < riWX')

            ariWX = oneSidedARI(W,X)
            ariWY = oneSidedARI(W,Y)
            ariWZ = oneSidedARI(W,Z)
            ariXY = oneSidedARI(X,Y)
            ariXZ = oneSidedARI(X,Z)
            ariYZ = oneSidedARI(Y,Z)

            self.assertTrue(ariYZ < ariXZ, 'ariYZ < ariXZ')
            self.assertTrue(ariXZ < ariWZ, 'ariXZ < ariWZ')
            self.assertTrue(ariWZ < ariWY, 'ariWZ < ariWY')
            self.assertTrue(ariWY < ariXY, 'ariWY < ariXY')
            self.assertTrue(ariXY < ariWX, 'ariXY < ariWX')

        def testOneSidedAriTrivialPartitions(self):
            p1 = [[1,2,3,4,5]]
            p2 = [[1,2,3,4,5]]
            self.assertEqual(1.0, oneSidedARI(p1,p2))

            p1 = [[4,5],[1,2,3]]
            p2 = [[1,2,3],[4,5]]
            self.assertEqual(1.0, oneSidedARI(p1,p2))

    class TestMutualInformation(TestCaseX):

        def testMI(self):
            X = list(range(5))
            S = [[x] for x in X]
            A = [[0,1],[2],[3,4]]

            self.assertEqual(0, mutualInformation([X],[X]), 'X-X')
            self.assertEqual(0, mutualInformation(S,  [X]), 'S-X')
            self.assertClose(np.log2(len(S)), mutualInformation(S,  S), msg='S-S')
            self.assertClose(_entropy(A), mutualInformation(A,A), msg='A-A')
            
        def testAMI(self):
            '''
            Test checks that ordering is as according to ordering in the paper
            by Gates and Ahn (2017, JMLR).
            '''
            W = [[1,2,3,4,5],[6,7,8,9,10],[11,12,13,14,15],[16,17,18,19,20]]
            X = [[1,2,3,4,5,6],[7,8,9,10],[11,12,13,14,15],[16,17,18,19,20]]
            Y = [[1,2],[3,4],[5,6],[7,8],[9,10],
                 [11,12],[13,14],[15,16],[17,18],[19,20]]
            Z = [[1],[2],[3],[4],[5],[6],[7],[8],[9,10],
                 [11,12,13,14,15,16,17,18,19,20]]

            miWX = mutualInformation(W,X)
            miWY = mutualInformation(W,Y)
            miWZ = mutualInformation(W,Z)
            miXY = mutualInformation(X,Y)
            miXZ = mutualInformation(X,Z)
            miYZ = mutualInformation(Y,Z)

            self.assertTrue(miXZ < miWZ)
            self.assertTrue(miWZ < miWY)
            self.assertTrue(miWY < miWX)
            self.assertTrue(miWX < miXY)
            self.assertTrue(miXY < miYZ)

            amiWX = oneSidedAMI(W,X)
            amiWY = oneSidedAMI(W,Y)
            amiWZ = oneSidedAMI(W,Z)
            amiXY = oneSidedAMI(X,Y)
            amiXZ = oneSidedAMI(X,Z)
            amiYZ = oneSidedAMI(Y,Z)

            self.assertTrue(amiYZ <= amiXZ, '%f vs %f' % (amiYZ,amiXZ))
            self.assertTrue(amiXZ <= amiWZ, '%f vs %f' % (amiXZ,amiWZ))
            self.assertTrue(amiWZ <= amiWY, '%f vs %f' % (amiWZ,amiWY))
            self.assertTrue(amiWY <= amiXY, '%f vs %f' % (amiWY,amiXY))
            self.assertTrue(amiXY <= amiWX, '%f vs %f' % (amiXY,amiWX))

    class TestOrderARI(TestCaseX):

        def testSame(self):
            from numpy.random import random
            R = random((10,10))
            A = np.array(R < 0.4, dtype=int)
            S = np.array((R < 0.4) | (R.T < 0.4), dtype=int)
            oari = orderARI(S,S)
            self.assertEqual(1.0,oari)

        def testOther(self):
            Q1 = [[1,2],[3],[3],[4],[]]   # Banjo
            Q2 = [[1,2,3],[3],[3],[4],[]] # Stringed banjo
            A1 = self._quivToAdj(Q1)
            A2 = self._quivToAdj(Q2)
            RI = orderARI(A1,A2)
            IR = orderARI(A2,A1)
            self.assertEqual(RI,IR)
            self.assertTrue(RI < 1.0, 'Was %f' % RI)

        def _quivToAdj(self,Q):
            N = len(Q)
            A = np.zeros((N,N),dtype=int)
            for i in range(N):
                for j in Q[i]:
                    A[i,j] = 1

            return A
            
    import logging
    logging.getLogger().setLevel(logging.ERROR)
    ut.main()

    
