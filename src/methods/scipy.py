# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 



def run_hc(G,D,config):
    import scipy.cluster.hierarchy as hc
    import time
    import methods.tools as tools

    log = tools.getLogger(run_hc)

    log.info('Running HC with config %s', config)

    if 'plus_dissim' in config and config['plus_dissim']:
        D = tools.make_plus_dissim(D,G)
    
    N = D.shape[0]
    d = []
    for i in range(N):
        for j in range(i+1,N):
            d.append(D[i,j])

    start = time.time()
    Z = hc.linkage(d,method=config['L'])
    log.info('HC spent %1.2f seconds.' % (time.time() - start))

    return _scipy_Z_to_ac(Z)


def _scipy_Z_to_ac(Z):
    '''
    Converts a linkage matrix to an AgglomerativeClustering object.
    '''
    from ophac.dtypes import AC
    import numpy as np
    
    n     = len(Z) + 1 # space size
    idMap = [i for i in range(n)] # maps from scipy cluster id to AC cluster index
    dists = []
    joins = []
    for i in range(len(Z)):
        idA,idB = Z[i][:2]
        join    = tuple(sorted((int(idMap[int(idA)]),idMap[int(idB)])))
        joins.append(join)
        dists.append(Z[i][2])
        idMap.append(np.min(join))
        for j in range(len(idMap)):
            if idMap[j] > join[1]:
                idMap[j] -= 1
            elif idMap[j] == join[1]:
                idMap[j] = join[0]
                        
    return AC(joins,dists)
