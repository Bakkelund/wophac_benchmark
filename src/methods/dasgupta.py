# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2022 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


def run_Dasgupta(G,D,config):
    import whophac_approx as wophac
    import treetools      as tt
    import numpy as np
    import time
    import methods.tools as tools
    import methods.wophac as wophac

    log = tools.getLogger(run_Dasgupta)
    assert np.all((0.0 <= D) & (D <= 1.0))

    if config['plus_dissim']:
        log.info('plus-dissim.')
        D = tools.make_plus_dissim(D,G)

    log.info('Running Dasgupta by setting W=0 and alpha=1.0...')
    W = {i:[] for i in range(D.shape[0])}
    return wophac.run_wophac(W,D,alpha=1.0)
