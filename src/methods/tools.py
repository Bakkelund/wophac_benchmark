# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2022 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


def getLogger(x):
    import logging
    return logging.getLogger(x.__module__ + '.' + x.__name__)


def make_plus_dissim(D,G):
    '''
    Assuming G is transitively closed
    '''
    import numpy as np
    
    DD = np.array(D)
    for i in sorted(G):
        for j in G[i]:
            DD[i,j] = 1.0
            DD[j,i] = 1.0

    return DD
