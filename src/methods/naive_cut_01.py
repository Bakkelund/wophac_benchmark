# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2022 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


def run_naive_cut(G,D,alpha):
    import rnd_split_shuffle as cutter
    import numpy             as np
    import treetools         as tt
    import timer
    import methods.tools     as tools
    
    log = tools.getLogger(run_naive_cut)
    log.info('Running naive dense cut on %d element set with alpha=%f', len(G), alpha)

    N = len(G)
    
    W = np.zeros((N,N))
    for i in range(N):
        for j in G[i]:
            W[i,j] = 1.0

    F = alpha*D + (1-alpha)*(W-W.T)
            
    tim = timer.timer()
    T  = cutter.hierarchy(F,cutter=cutter.density_cut_value)
    log.info('rnd_split_shuffle took %1.1f s.', tim)

    return tt.treeToOphacAC(T)
