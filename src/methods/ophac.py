# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

    
def run_ophac(G,D,config):
    import ophac.hierarchy as hc
    import time
    import methods.tools as tools

    L       = config['L']
    ncppcpu = config['ncppcpu']
    
    log = tools.getLogger(run_ophac)

    log.info('Running ophac with L=%s', L)
    
    q = [sorted(G[i]) for i in sorted(G)]
    d = []
    for i in range(len(G)):
        for j in range(i+1,len(G)):
            d.append(D[i,j])

    start = time.time()
    acs   = hc.approx_linkage(D=d, G=q, L=L, n=20, procs=ncppcpu)
    log.info('ophac spent %1.2f seconds.' % (time.time() - start))

    if len(acs) > 1:
        log.info('%d results for ophac --- picking #0' % len(acs))

    return acs[0]
