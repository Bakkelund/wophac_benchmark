# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def run_wophac(G,D,alpha):
    import whophac_approx as wophac
    import treetools      as tt
    import numpy as np
    import time
    import methods.tools as tools

    assert np.all((0.0 <= D) & (D <= 1.0))

    log = tools.getLogger(run_wophac)
    log.info('Running wophac on %d element set with alpha=%f', len(G), alpha)

    N = len(G)
    S = 1.0 - D
    
    W = np.zeros((N,N))
    for i in range(N):
        for j in G[i]:
            W[i,j] = 1.0
            
    tim = time.time()
    T  = wophac.hierarchy(S,W,alpha,cutter=wophac.dir_sparse_cut)
    log.info('wophac took %1.1f s.', time.time() - tim)

    return tt.treeToOphacAC(T)
