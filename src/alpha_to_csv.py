# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def pop_arg(key, required=True, default=None):
    import sys
    for i in range(len(sys.argv)):
        if sys.argv[i].startswith(key + '='):
            res = sys.argv[i][(len(key)+1):]
            del sys.argv[i]
            return res

    if (default is None) and (required is True):
        raise Exception('Missing command line argument "%s"' % key)

    return default

def method_ofname(ofname,method):
    base,ext = ofname.split('.')
    return base + '_' + method + '.' + ext

def write(X,Y,ofname):
    with open(ofname, 'w') as outf:
        for x,y in zip(X,Y):
            outf.write('%e %e\n' % (x,y))

            
if __name__ == '__main__':
    import numpy as np
    import sys
    import json
    import dumptools   as dut
    import plot_on_dim as pod

    qual   = pop_arg('qual')
    ofname = pop_arg('ofname')

    with open(sys.argv[1], 'r') as inf:
        data = json.load(inf)

    methods = ['ophac','scipy','scipyM', 'dgM']
    m_data  = [dut.get_performance(data[m]) for m in methods]
        
    astrs  = sorted(data['wophac'])
    wophac = [dut.get_performance(data['wophac'][a]) for a in astrs]
    alphas = [float(a) for a in astrs]
        
    a0     = alphas[0]
    a1     = alphas[-1]
    
    for m,md in zip(methods,m_data):
        mval = np.mean(md[qual])
        oname = method_ofname(ofname,m)
        write([a0,a1],[mval,mval],method_ofname(ofname,m))

    wops = [np.mean(w[qual],axis=0) for w in wophac]
    write(alphas,wops,method_ofname(ofname,'wophac'))

    if qual == 'loops':
        loops = [np.min(w[qual],axis=0) for w in wophac]
        write(alphas,loops,method_ofname(ofname,'wophac_loops'))
    
