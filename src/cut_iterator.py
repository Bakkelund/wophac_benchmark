# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

import numpy     as np

def trees(x):
    if len(x) == 1:
        yield {}
        return
    
    for a,b in cuts(x):
        joins = {tuple(sorted((i,j))):(a,b) for i in a for j in b}
        for ja in trees(a):
            for jb in trees(b):
                yield {**joins,**ja,**jb}
        
def cuts(x):
    if len(x) <= 1:
        yield x,[]
        return
    
    X = np.array(x)
    for select in _binarrays(len(x)):
        A = X[select]
        B = X[~select]
        yield (A,B)
        

def _binarrays(n):
    B     = np.zeros((n,), dtype=bool)
    B[-1] = True
    for _ in range(2**(n-1) - 1):
        yield B
        B = _inc(B)

def _inc(B):
    ids = np.where(B==0)[-1]
    n   = 0
    if len(ids) > 0:
        n = ids[-1]
    
    B[n:] = ~B[n:]
    return B
