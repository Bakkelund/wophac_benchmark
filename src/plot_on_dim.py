# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def pick_alpha():
    import sys
    for i in range(len(sys.argv)):
        if sys.argv[i].startswith('alpha='):
            alpha = float(sys.argv[i][len('alpha='):])
            del sys.argv[i]
            return alpha

    return None

def pick_ylims():
    import sys
    for i in range(len(sys.argv)):
        if sys.argv[i].startswith('ylims='):
            vals = sys.argv[i][len('ylims='):]
            del sys.argv[i]
            return [float(y) for y in vals.split(',')]

    return None

def pick_boxes():
    import sys
    bx1 = None
    bx2 = None
    for i in range(len(sys.argv))[::-1]:
        if sys.argv[i].startswith('bx1='):
            bx1 = sys.argv[i][len('bx1='):]
            del sys.argv[i]
            continue
        if sys.argv[i].startswith('bx2='):
            bx2 = sys.argv[i][len('bx2='):]
            del sys.argv[i]
            continue

    return bx1,bx2

def pick_qualities():
    import sys
    import dumptools as dut
    for i in range(len(sys.argv)):
        if sys.argv[i].startswith('quals='):
            qual = sys.argv[i][len('quals='):]
            del sys.argv[i]
            res = qual.split(',')
            for r in res:
                if not r in dut.QUALITIES:
                    raise Exception('Unknown quality: "%s"' % r)

            return res

    return dut.QUALITIES

def is_grouped():
    import sys
    if 'grouped' in sys.argv:
        ig = sys.argv.index('grouped')
        del sys.argv[ig]
        return True
    else:
        return False

def get_dim():
    import sys
    for i in range(len(sys.argv)):
        if sys.argv[i].startswith('dim='):
            dim = sys.argv[i][len('dim='):]
            del sys.argv[i]
            return dim

    return 'var'

def get_dim_picker(dim):
    import dumptools as dut
    return lambda fname : dut.dim_from_fname(dim,fname)
    
def rel_var_box(method_vals, m1, m2, qual):
    import numpy as np
    varis = np.array(sorted(method_vals[m1]), dtype=float)
    vals1 = np.array([method_vals[m1][v][qual] for v in varis])
    vals2 = np.array([method_vals[m2][v][qual] for v in varis])
    diff  = np.abs(vals1-vals2)
    dvar  = np.var(diff, axis=1)
    return np.sqrt(dvar)

def var_box(method_vals, m, qual):
    import numpy as np
    varis = np.array(sorted(method_vals[m]), dtype=float)
    vals  = np.array([method_vals[m][v][qual] for v in varis])
    dvar  = np.var(vals,axis=1)
    return np.sqrt(dvar)

def title_from_fname(fname):
    import os.path
    return os.path.split(fname)[-1]


def method_names():
    import sys
    import json
    import dumptools 
    with open(sys.argv[1],'r') as inf:
        config = json.load(inf)
    a,b = dumptools.extract_method_names(config['config'])
    return [*a,*b]

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from collections import defaultdict
    import numpy as np
    import sys
    import json
    import dumptools as dut
    import colors

    dim          = get_dim()
    dim_picker   = get_dim_picker(dim)
    alpha        = pick_alpha()
    mbox1, mbox2 = pick_boxes()
    qualities    = pick_qualities()
    ylims        = pick_ylims()

    if not is_grouped():
        assert dut.same_dim_files(dim, sys.argv[1:])

    methods       = method_names()
    method_values = defaultdict(lambda : {})
    for fname in sys.argv[1:]:
        print('Reading', fname)
        dval = dim_picker(fname)
        fres = dut.load_on_alpha(fname,alpha)
        for m in methods:
            method_values[m][dval] = fres[m]

    rows    = len(qualities)
    cols    = 1
    fig,axs = plt.subplots(rows,cols)
    axs     = np.reshape(axs,(rows*cols,))

    fig.canvas.manager.set_window_title(title_from_fname(fname))

    cmap  = colors.ColorMap(methods)
    varis = sorted(method_values[methods[0]])
    for ax,qual in zip(axs,qualities):
        ax.set_title('%s mean values along %s' % (qual,dim))
        ax.set_ylabel(qual)
        ax.set_xlabel(dim)

        if ylims:
            ax.set_ylim(*ylims)
        
        for m in methods:
            mvals = np.array([method_values[m][v][qual] for v in varis])
            mean  = np.mean(mvals, axis=1)
            ax.plot(varis,mean,'-x',label=m,color=cmap[m])
            if m == mbox1:
                if mbox2:
                    std = rel_var_box(method_values, mbox1, mbox2, qual)
                else:
                    std = var_box(method_values, mbox1, qual)
                ax.plot(varis,mean-std,'--',color=cmap[m])
                ax.plot(varis,mean+std,'--',color=cmap[m])

        ax.legend(loc='lower left')

    plt.tight_layout()
    plt.show()
