# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2022 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

import numpy as np

class ColorMap:
    # Colors from the palette defined at
    # https://www.schemecolor.com/high-contrast.php
    BLUE_VIOLET     = np.array((137, 49, 239))/255
    JONQUIL         = np.array((242, 202, 25))/255
    SHOCKING_PINK   = np.array((255, 0, 189))/255
    RYB_BLUE        = np.array((0, 87, 233))/255
    ALIEN_ARMPIT    = np.array((135, 233, 17))/255
    SPANISH_CRIMSON = np.array((225, 24, 69))/255

    COLORS = [BLUE_VIOLET, JONQUIL, SHOCKING_PINK,
              RYB_BLUE, ALIEN_ARMPIT, SPANISH_CRIMSON]
    
    def __init__(self,methods):
        from itertools import cycle
        M     = len(methods)
        N     = len(self.COLORS)
        delta = 1/np.ceil(M/N)
        self.colors = {}
        for i in range(M):
            m = methods[i]
            a = np.floor(i/N)
            self.colors[m] = (1-a*delta)*self.COLORS[i%N] + a*delta*self.COLORS[(i+1)%N]
        
    def color(self,m):
        return self.colors[m]
        
    def __getitem__(self,m):
        return self.colors[m]
