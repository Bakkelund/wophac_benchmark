# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def _getLogger(x):
    import logging
    return logging.getLogger(__name__ + '.' + x.__name__)

def _set_logging(json_config):
    import logging
    
    params    = _bind(json_config).system
    confLevel = getattr(logging,params.logLevel.upper())
    logging.basicConfig(level=confLevel)

    logging.getLogger('ophac.hierarchy').setLevel(logging.INFO)
    logging.getLogger('ophac.hierarchy._rndpick_linkage').setLevel(confLevel)
    logging.getLogger(__name__).setLevel(logging.INFO)

def gen_problem(ccs, n0, minDeg, m, mu, var):
    '''
    Generates a planted partition problem.
    ccs    - list of cc instances to use for base space sampling
    n0     - cardinality of base space
    minDeg - minimum in/out degree of base space sample
    m      - number of copies
    mu     - location for noise in copying
    var    - variance for noise in copying

    Returns (X,D,PP,QQ) where 

    X  is the problem space as a transitively closed dict graph,
    D  is the dissimilarity coefficients in an NxN numpy matrix
    PP is the planted partition as a list of lists of indices
    QQ is the relation induced by PP
    '''
    import machine_parts_pp as pp

    assert m >= 1
    
    cc = pp.get_machine_part_connected_components(ccs)
    g0 = dict(cc[0])
    for c in cc[1:]:
        g0.update(c)

    G      = pp.random_induced_subgraph(g0,n0,minDeg)
    G,D,PP = pp.planted_partition(G,m-1,mu,var)
    G      = pp._transitive_closure(G)
    QQ     = induced_relation(G,PP)
    
    return G,D,PP,QQ

def induced_relation(G0,PP):
    '''
    Assumes G0 is transitively closed
    '''
    from itertools import product
    from itertools import combinations as pick
    
    N  = len(PP)
    QQ = [list() for _ in range(N)]
    for i in range(N):
        for j in range(i+1,N):
            pi = PP[i]
            pj = PP[j]
            for a,b in product(pi,pj):
                if b in G0[a]:
                    QQ[i].append(j)
                if a in G0[b]:
                    QQ[j].append(i)

    return [sorted(set(q)) for q in QQ]

def compute_base_space_projection(G,PP):
    from itertools import combinations as pick
    from itertools import product

    QQ = [list() for _ in range(len(G))]
    for c1,c2 in pick(PP,2):
        rightArrow = False
        leftArrow  = False
        for a,b in product(c1,c2):
            if b in G[a]:
                rightArrow = True
            if a in G[b]:
                leftArrow = True

            if leftArrow and rightArrow:
                break

        if rightArrow:
            for a,b in product(c1,c2):
                QQ[a].append(b)

        if leftArrow:
            for a,b in product(c1,c2):
                QQ[b].append(a)

    # Remove duplicates and sort
    return sorted([sorted(set(q)) for q in QQ])

def _par_solve_problem(DATA):
    '''
    For each iteration
    '''
    import ophac.rnd
    import dumptools        as dut
    import comp_anal_loader as cal
    import numpy            as np
    import json
    import ari_facade       as arif

    configs,zeed = DATA
    json_config  = json.loads(configs)
    _set_logging(json_config)
    
    log    = _getLogger(_par_solve_problem)
    config = _bind(json_config)

    log.info('Running sub-process with seed %d.', zeed)

    ccs    = config.problem.ccs
    n0     = config.problem.subsetsize
    m      = config.problem.copies
    minDeg = config.problem.minDeg
    mu     = config.problem.mu
    var    = config.problem.var

    a0     = config.problem.alphas.a0
    a1     = config.problem.alphas.a1
    an     = config.problem.alphas.n
    alphas = np.linspace(a0, a1, an)
        
    ophac.rnd.seed(zeed)
    
    result = {'config':json_config}

    try:
        G,D,PP,QQ = gen_problem(ccs, n0, minDeg, m, mu, var)
    except Exception as e:
        log.exception('Skipping execution of %s due to exception:', DATA)
        return None

    evaluator = arif.ClustEval(gold=PP,
                               qGold=QQ,
                               q0=[sorted(G[i]) for i in sorted(G)],
                               quals=json_config['qualities'])

    runners = cal.load(config=json_config)
    for m,mres in runners.run_alphas(G,D,alphas):
        result[m] = {}
        for alpha,ac in zip(alphas,mres):
            key = dut.flt_as_key(alpha)
            result[m][key] = evaluator.eval(ac)

    for m,ac in runners.run_others(G,D):
        result[m] = evaluator.eval(ac)

    return result


def dump(json_config):
    from concurrent.futures import ProcessPoolExecutor as Pool
    from numpy.random import randint
    import numpy as np
    from collections import defaultdict
    import dumptools as dt
    import time
    import json
    import json

    log  = _getLogger(dump)
    log.info('Dumping %s', json_config)

    config = _bind(json_config)

    
    zeed   = lambda : randint(2**32-1)
    start  = time.time()

    
    sconf  = json.dumps(json_config)
    inputs = [(sconf,zeed()) for _ in range(config.problem.iters)]
    log.info('Spawning off %d processes on %d CPUs.',
             len(inputs), config.system.ncpu)

    with Pool(config.system.ncpu) as pool:
        results = pool.map(_par_solve_problem, inputs)

    duration = time.time() - start
    log.info('Total execution time: %1.1f s.', duration)
    
    other_names, wophac_names = dt.extract_method_names(json_config)
    log.info('Methods to dump: %s', ','.join([*other_names, *wophac_names]))
    
    quals    = config.qualities
    base_res = lambda : {q:list() for q in quals}
    result   = {m:base_res() for m in other_names}
    for wt_name in wophac_names:
        result[wt_name] = defaultdict(base_res)
    for itres in results:

        if itres is None:
            # process terminated with exception
            continue
        
        for method in [*other_names, *wophac_names]:
            if method not in wophac_names:
                for quality in quals:
                    result[method][quality].append(itres[method][quality])
            elif method in wophac_names:
                for alpha in itres[method]:
                    for quality in quals:
                        val = itres[method][alpha][quality]
                        result[method][alpha][quality].append(val)
            else:
                raise Error('Unknown method: %s' % method)    
                    
    result['config'] = json_config
    result['time']   = duration

    ofname = dt.get_dump_filename(json_config)
    log.info('Duming the result to "%s"', ofname)
    with open(ofname, 'w') as outf:
        json.dump(result, outf, indent=3)

def _bind(y):
    from collections import namedtuple
    import copy
    x = copy.deepcopy(y)
    for a,b in x.items():
        if isinstance(b,dict):
            x[a] = _bind(b)

    return namedtuple('object_name', x.keys())(*x.values())
    
if __name__=='__main__':
    import json
    import sys
    import dumptools as dt

    json_config = dt.default_json_config()
    if len(sys.argv) > 2 and sys.argv[1] != 'fast':
        with open(sys.argv[1], 'r') as inf:
            ext_config = json.load(inf)
            json_config.update(ext_config)

    if 'fast' in sys.argv:
        json_config['problem']['iters']       = 1
        json_config['problem']['subsetsize']  = 4
        json_config['problem']['copies']      = 4
        json_config['problem']['alphas']['n'] = 3
        
    _set_logging(json_config)

    config = _bind(json_config)
    params = config.system
        
    if params.seed != -1:
        import ophac.rnd
        ophac.rnd.seed(params.seed)
    
    dump(json_config)
