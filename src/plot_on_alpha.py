# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

import dumptools as dut

def getQualities():
    import sys
    for i in range(len(sys.argv)):
        if sys.argv[i].startswith('quals='):
            quals = sys.argv[i][len('quals='):].split(',')
            del sys.argv[i]
            return quals

    return dut.QUALITIES

def get_ylim():
    import sys
    for i in range(len(sys.argv)):
        if sys.argv[i].startswith('ylim='):
            lims = sys.argv[i][len('ylim='):].split(',')
            del sys.argv[i]
            return [float(y) for y in lims]
            
    return None
            
if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import dumptools as dt
    import numpy as np
    import sys
    import json
    import colors

    qualities = getQualities()
    ylim      = get_ylim()
    
    with open(sys.argv[1], 'r') as inf:
        data = json.load(inf)

    dump_config = data['config']
        
    methods,wt_methods = dt.extract_method_names(dump_config)
    
    m_data     = [dut.get_performance(data[m]) for m in methods]
        
    astrs   = sorted(data[wt_methods[0]])
    wt_data = [[dut.get_performance(data[m][a]) for a in astrs] for m in wt_methods]
    alphas  = [float(a) for a in astrs]
        
    a0      = alphas[0]
    a1      = alphas[-1]

    rows    = len(qualities)
    cols    = 1
    fig,axs = plt.subplots(rows,cols)
    axs     = np.reshape(axs,(rows*cols,))
    fig.canvas.manager.set_window_title(sys.argv[1])

    cm = colors.ColorMap([*methods,*wt_methods])
    
    for ax,qual in zip(axs,qualities):
        ax.set_title('%s mean values' % qual)
        ax.set_ylabel(qual)
        ax.set_xlabel('alpha')
        #ax.set_ylim(0,1)
        for m,md in zip(methods,m_data):
            mval = np.mean(md[qual])
            mrk  = '.'
            if m == 'ophac':
                mrk='x'

            ax.plot([a0,a1],[mval,mval],label=m,marker=mrk,color=cm[m])
            if ylim:
                ax.set_ylim(*ylim)

        for wtm,wtd in zip(wt_methods,wt_data):
            wops = [np.mean(w[qual],axis=0) for w in wtd]
            ax.plot(alphas,wops, label=wtm, marker=mrk, color=cm[wtm])
            ax.legend(loc='right')

    plt.tight_layout()
    plt.show()
