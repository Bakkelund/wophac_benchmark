# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def _getLogger(x):
    import logging
    return logging.getLogger(__name__ + '.' + x.__name__)

class ClustEval:
    def __init__(self, gold, qGold, q0, quals):
        self.quals = quals
        self.gold  = gold
        self.qGold = qGold
        self.q0    = q0
        self.log   = _getLogger(ClustEval)
        self.log.info('Set up for computing %s', ','.join(self.quals))

    def eval(self,ac):
        import clust_eval
        import ophac.dtypes as dt

        self.log.debug('Evaluating %s', ac)
        
        N = len(self.q0)
        P = dt.Partition(n=N)
        Q = dt.Quivers(self.q0)

        bestP = P
        bestQ = Q

        ari  = clust_eval.oneSidedARI(self.gold, P.data, 'all')
        for join in ac.joins:
            P    = dt.merge(P,[join])
            Q    = dt.merge(Q,[join])
            ari2 = clust_eval.oneSidedARI(self.gold, P.data, 'all')
            if ari2 > ari:
                ari  = ari2
                bestP = P
                bestQ = Q

        result = {}
                
        if 'ari' in self.quals:
            result['ari'] = ari

        if 'oari' in self.quals:
            O0   = _induced_on_base(self.gold,self.qGold,N)
            O1   = _induced_on_base(bestP,bestQ,N)
            oari = clust_eval.orderARI(O0,O1)
            result['oari'] = oari

        if 'loops' in self.quals:
            loop = loopsOnBase(bestP,bestQ,N)
            result['loops'] = loop
        
        return result

def loopsOnBase(P,Q,N):
    import clust_eval
    
    R  = _induced_on_base(P,Q,N)
    Q0 = [list() for _ in range(R.shape[0])]
    for i in range(R.shape[0]):
        for j in range(R.shape[0]):
            if R[i,j] > 0:
                Q0[i].append(j)

    return clust_eval.loopFactor(Q0)

def _induced_on_base(P,Q,N):
    from itertools import product
    import ophac.dtypes as dt
    import numpy as np

    Q = dt.Quivers(Q).transitiveClosure(lenient=True).quivers

    assert len(P) == len(Q)
    
    log = _getLogger(_induced_on_base)
    log.debug('N:%d\nP: %s\nQ: %s', N,P,Q)

    m = len(P)
    R = np.zeros((N,N), dtype=int)
    for i in range(len(Q)):
        for j in Q[i]:
            I,J = zip(*product(P[i],P[j]))
            R[I,J] = 1
            
    return R



