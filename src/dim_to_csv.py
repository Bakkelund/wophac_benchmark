# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def pop_arg(key, required=True, default=None):
    import sys
    for i in range(len(sys.argv)):
        if sys.argv[i].startswith(key + '='):
            res = sys.argv[i][(len(key)+1):]
            del sys.argv[i]
            return res

    if required:
        raise Exception('Missing command line argument "%s"' % key)

    return default

if __name__ == '__main__':
    from collections import defaultdict
    import numpy as np
    import sys
    import json
    import dumptools   as dut
    import plot_on_dim as pod

    dim        = pod.get_dim()
    dim_picker = pod.get_dim_picker(dim)
    alpha      = pod.pick_alpha()
    qual       = pop_arg('qual')
    ofname     = pop_arg('ofname')
    method     = pop_arg('method')
    
    if not pod.is_grouped():
        assert dut.same_dim_files(dim, sys.argv[1:])

    values = {}
    for fname in sys.argv[1:]:
        print('Reading', fname)
        dval = dim_picker(fname)
        fres = dut.load_on_alpha(fname,alpha)
        values[dval] = fres[method]
    
    varis = sorted(values)
    vals  = np.array([values[v][qual] for v in varis])
    means = np.mean(vals, axis=1)
    with open(ofname, 'w') as outf:
        for x,y in zip(varis,means):
            outf.write('%e %e\n' % (x,y)) 
    

    if qual == 'loops' and method == 'wophac':
        mins = np.min(vals, axis=1)
        assert ofname.find('loops.') != -2
        ofn2 = ofname.replace('loops.', 'loops_min.')
        with open(ofn2, 'w') as outf:
            for x,y in zip(varis,mins):
                outf.write('%e %e\n' % (x,y)) 
        
