
def _getLogger(x):
    import logging
    return logging.getLogger(__name__ + '.' + x.__name__)

class OtherRunner:
    def __init__(self,name,method,config):
        self.log    = _getLogger(OtherRunner)
        self.name   = name
        self.call   = method
        self.config = config

    def is_alpha(self):
        return False
    
    def __call__(self,G,D):
        if self.config:
            self.log.info('Running %s with config.', self.name)
            return self.call(G,D,self.config)
        else:
            self.log.info('Running %s without config.', self.name)
            return self.call(G,D)

class AlphaRunner(OtherRunner):
    def __init__(self,name,method,config):
        OtherRunner.__init__(self,name,method,config)
        
    def is_alpha(self):
        return True

    def __call__(self,G,D,a):
        if self.config:
            self.log.info('Running %s with config.', self.name)
            return self.call(G,D,a,self.config)
        else:
            self.log.info('Running %s without config.', self.name)
            return self.call(G,D,a)
        
class Runners:
    def __init__(self):
        self.alpha_runners = []
        self.other_runners = []

    def add_runner(self,runner):
        if isinstance(runner,AlphaRunner):
            self.alpha_runners.append(runner)
        elif isinstance(runner,OtherRunner):
            self.other_runners.append(runner)
        else:
            raise Exception('Unknown runner type: %s' % str(type(runner)))

    def run_alphas(self,G,D,alphas):
        for runner in self.alpha_runners:
            yield (runner.name,[runner(G,D,a) for a in alphas])

    def run_others(self,G,D):
        for runner in self.other_runners:
            yield (runner.name,runner(G,D))

def load(fname=None,config=None):
    import json
    from importlib import import_module

    assert (fname is None) != (config is None)

    if config is None:
        with open(fname, 'r') as inf:
            config = json.load(inf)

    runners = Runners()
    methods = config['methods'] 
    for m in methods:
        if (not 'disabled' in methods[m]) or (not methods[m]['disabled']):
            module = import_module(methods[m]['module'])
            callpt = getattr(module, methods[m]['callpoint'])
            
            cpconf = None
            if 'config' in methods[m]:
                cpconf = methods[m]['config']

            RunnerType = OtherRunner
            if 'alpha' in methods[m] and methods[m]['alpha']:
                RunnerType = AlphaRunner

            runners.add_runner(RunnerType(m,callpt,cpconf))

    return runners
