# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

QUALITIES   = ('ari','oari','loops')

def flt_as_key(flt):
    return '%1.5f' % flt

def color(method):
    colors = ['blue' ,'orange','green' ,'purple','red','brown']
    assert len(colors) == len(METHODS)
    color_map = {m:c for m,c in zip(METHODS,colors)}
    return color_map[method]


def get_performance(case):
    import numpy as np
    result = {}
    for qual in QUALITIES:
        result[qual] = np.array(case[qual])

    return result

def same_dim_files(dim,fnames):
    reduced_fnames = set()
    for fname in fnames:
        a,b,_,c = split_fname_dim(dim,fname)
        reduced_fnames.add(a+b+c)
        
    return len(reduced_fnames) == 1

def same_var_files(fnames):
    return same_dim_files('var', fnames)

def split_fname_var(fname):
    return split_fname_dim('var', fname)
    
def split_fname_dim(dim,fname):
    import re
    rx = r'^(.*_)((' + dim + r')(\d+))(.*)$'
    m  = re.match(rx, fname)

    if not m:
        raise Exception('"%s" not extractable from "%s"' % (dim,fname))
    
    return [m.group(1),m.group(3),m.group(4),m.group(5)]

def get_dump_filename(json_config):
    '''
    Takes the passed filename and adds config info to the fname to
    make it eatable for the plot functions.
    '''
    import os.path as path

    conf_fname = json_config['data']['ofname']
    dirname,basename = path.split(conf_fname)
    root,ext         = path.splitext(basename)

    def toStr(x):
        s = '%1.3f' % x
        i = s.index('.')
        return s[:i] + s[(i+1):]
    
    P   = json_config['problem']
    n   = P['subsetsize']
    m   = P['copies']
    md  = toStr(P['minDeg'])
    mu  = toStr(P['mu'])
    var = toStr(P['var'])
    
    decoration = '_n%d_m%d_minDeg%s_mu%s_var%s' % (n,m,md,mu,var)

    return path.join(dirname,root + decoration + ext)

def dim_from_fname(dim,fname):
    _,_,vstr,_ = split_fname_dim(dim,fname)
    flt_str = vstr[0] + '.' + vstr[1:]
    return float(flt_str)

def var_from_fname(fname):
    return dim_from_fname('var', fname)

def load_on_alpha(fname,alpha):
    import json
    import numpy as np

    if alpha is None:
        alpha = find_best_alpha(fname)
        print('Best alpha:', alpha)
    
    with open(fname, 'r') as inf:
        data = json.load(inf)

    others,alphas = extract_method_names(data['config'])
        
    result   = {m:get_performance(data[m]) for m in others}
    alphastr = flt_as_key(alpha)
    for m in alphas:
        result[m] = get_performance(data[m][alphastr])

    return result

def find_best_alpha(fname):
    import json
    import numpy as np
    
    var = var_from_fname(fname)
    with open(fname, 'r') as inf:
        data = json.load(inf)

    alphas = sorted(data['wophac'])
    means  = np.array([np.mean(data['wophac'][x]['ari']) for x in alphas])
    #iMax   = np.argmax(means)
    iMax   = np.where(means == means.max())[0][-1]
    return float(alphas[iMax])
    

def default_json_config():
    import sys
    import json
    import os.path as path

    main_fname = sys.modules[__name__].__file__
    dirname    = path.dirname(main_fname)
    conf_fname = path.join(dirname, 'comp_anal_setup.json')
    with open(conf_fname, 'r') as inf:
        config = json.load(inf)

    return config


def extract_method_names(json_config):
    others  = []
    alphas  = []
    methods = json_config['methods']
    for m in methods:
        if 'alpha' in methods[m] and methods[m]['alpha']:
            alphas.append(m)
        else:
            others.append(m)

    return others,alphas
