# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


PP1=${PWD}/src

python3 -m pip show ophac > /dev/null
if test "$?" != "0"; then
    echo ophac not found -- setting development path
    cd ../ophac_dev/ophac
    PP1=${PP1}:${PWD}/src
    cd -
else
    echo 'ophac alredy installed'
fi

python3 -m pip show machine-parts-pp > /dev/null
if test "$?" != "0"; then
    echo machine-parts-pp not found -- setting development path
    cd ../machine-parts-pp/python
    PP1=${PP1}:${PWD}/src
    cd -
else
    echo 'machine-parts-pp alredy installed'
fi

export PYTHONPATH=.:${PP1}

if test ! -d "./tmp"; then
    echo "Creating tmp directory."
    mkdir ./tmp
fi

