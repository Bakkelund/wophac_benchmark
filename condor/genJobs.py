# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Copyright 2021 Daniel Bakkelund
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

import numpy as np
import numpy.random as rnd
import time

rnd.seed(int(time.time()))

CPP_MAX_UINT = 4294967294

ccs      = [1,2,4,5,6,7] 
a0       = 0.0
a1       = 1.0
nalphas  = 11
minDegs  = [0.5, 1.0, 2.0]
scales   = [0.05,0.1,0.15,0.2,0.3,0.4]
L        = 'complete'
iters    = 30
NM       = [(4,5),(5,4),(6,3)]


def lnk(L):
    return {'single':'SL', 'average':'AL', 'complete':'CL'}[L]

def fltstr(s):
    return ('%0.2f' % s).replace('.','')

with open('condor.job', 'r') as inf:
    base_file = ''.join(inf.readlines())

for cc in ccs:
    for n,m in NM:
        for var in scales:
            for minDeg in minDegs:
                zeed  = rnd.randint(CPP_MAX_UINT)
                fbase = 'cc%d_n%d_m%d_minDeg%s_var%s' % (cc, n, m, fltstr(minDeg), fltstr(var))
                outfn = 'condor/data/wophac_bm_' + fbase + '.json'

                params = '--ccs:%d --n0:%d --m:%d --minDeg:%1.2f --var:%1.2f --L:%s --a0:%1.2f --a1:%d --nalphas:%d --iters:%d --fname:%s --seed:%d' % \
                    (cc, n, m, minDeg, var, L, a0, a1, nalphas, iters, outfn, zeed)
        
                jobf = base_file.replace('FBASE',fbase)
                jobf = jobf.replace('PARAMS', params)
        
                with open('jobs/condor_' + fbase + '.job', 'w') as outf:
                    outf.write(jobf)
            
            
