# wophac_benchmark

This project contains code for benchmarking of the method
described in <https://arxiv.org/abs/2109.04266>.

To run the code in this repository, you may need to install the following libraries via pip:

- numpy
- matplotlib
- scipy
- opahc
- clusim
- machine-parts-pp

**Note that everything in this repo is work in progress, and may change at any time.**

## License
The software in this module is licensed under the GNU Lesser General Public License ([https://www.gnu.org/licenses/lgpl-3.0.en.html](https://www.gnu.org/licenses/lgpl-3.0.en.html)).
